/**
 * Global object storing action handlers for actions related to tasks
 */
var actions = {
	/**
	 * Handles submit event on the main form of the application
	 * which is responsible for creation of new tasks. This 
	 * function creates a new task object and adds new task container
	 * to page.
	 */
	onSubmit : function( event ){
		let $input = $( this ).find('input').eq(0);
		let val = $input.val();
		$input.val('');

		if(val.length === 0)
			return false;

		var newTask = new Task(val);
		taskHandler.addNewToContainer(newTask, $('#activeTasks'));

		$input.focus();		
		event.preventDefault();
	},
	/**
	 * Handles task removal. 
	 */
	onRemove : function( event ){
		let $task = $( event.target ).parent();
		taskHandler.removeFromContainer($task);
		event.preventDefault();
	},
	/**
	 * Handles task completion,by moving top level task 
	 * container to the panel on the right
	 */
	onComplete : function( event ){				
		taskHandler.moveToNewContainer($( event.target ).parent(),$('#finishedTasks'));
		event.preventDefault();
	}, 
	/**
	 * Handles task edit functionality
	 */
	onEdit : function( event ){
		let $task = $( event.target ).parent();
		var taskObj = $task.data('task');

		taskHandler.togglePanels(taskObj,$task);		
		event.preventDefault();
	},
	/**
	 * Handles saving task details. Stores new details
	 * into a Task and stores the Task as a data for 
	 * a particular top level jQuery object.
	 */
	onSaveDetails : function( event ){
		let $form = $( event.target );
		let $task = $form.parents('div.task').eq(0);
		let taskObj = $task.data('task');
		let fields = $form.serializeArray();
		taskObj.description = fields[0].value;
		taskObj.dateExpires = fields[1].value;
		taskObj.color = fields[2].value;
		taskObj.hasDetails = true;
		$task.data('task', taskObj );
		
		$task.css({borderLeft: '10px solid '+taskObj.color});	
		taskHandler.togglePanels(taskObj,$task);
		event.preventDefault();
	}
};

/**
 * Assigns listeners to events happening on the page. 
 * Uses task delegation. Tasks are caught on main container to 
 * avoid assigning handlers to each newly created element on the page. 
 */
$( document ).ready( function() {	
	$('#newTaskForm').submit(actions.onSubmit);
	$('#container').on('click', 'a[class^="taskRemove"]', actions.onRemove);
	$('#container').on('click', 'a[class^="taskComplete"]', actions.onComplete);
	$('#container').on('click', 'a[class^="taskEdit"]', actions.onEdit);
	$('#container').on('submit', 'form[class^="taskDetailsForm"]', actions.onSaveDetails);
	$('#newTaskForm').children('input:first-child').focus();
});
