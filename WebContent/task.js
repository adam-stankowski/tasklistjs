/**
 * 
 */

/**
 * This "class" is a representation of a Task in the system
 * and carries all the necessary fields
 */
 function Task( textContent ){
 	this.textContent = textContent;	
 	this.dateExpires = null;
 	this.description = '';
 	this.color = '';
 	this.image = '';
 	this.hasDetails = false;
 }

 /**
  * taskBuilder is a public object which stores functions 
  * which allow building of various task-related html elements
  */
 var taskBuilder = {

	 /**
	  * Builds an anchor tag with a specified text in it and 
	  * belonging to a specific CSS class. Used when defining
	  * anchors to manipulate each task (Edit, mark done, remove)
	  */
 	buildTaskManipulationAnchor : function(aText,aClass){
 		let $me = $('<a>',
	 	{
	 		text: aText,
	 		href : '',
	 		'class': aClass
	 	});

	 	return $me;
 	},
 	/**
 	 * Builds html for aTask passed as a parameter and returns the
 	 * desired jQuery object of a top level container.
 	 */
 	buildNewTaskHTML : function(aTask){
 		let $me = $('<div>',{'class' : 'task'});
	 	$('<h3>',{text:aTask.textContent}).appendTo($me);	 	

	 	let buttons = [this.buildTaskManipulationAnchor('','taskRemove'),
	 	this.buildTaskManipulationAnchor('','taskComplete'),
	 	this.buildTaskManipulationAnchor('', 'taskEdit')];
	 	
		$.each(buttons, function(index, elem) {
			elem.appendTo($me);
		});

		$('<div>',{'style':'clear:both'}).appendTo($me);

		$('<div>', {'class' : 'taskDetails'}).appendTo($me);

		$me.data('task', aTask); // Add yourself as a data in DOM element
	 	return $me;
 	},
 	/**
 	 * Builds a form which is displayed when user clicks edit task button
 	 * and returns it as a jQuery object.
 	 */
 	buildTaskEditForm : function(aTask){
 		
	 	let $form = $('<form>',{'class' : 'taskDetailsForm'});
	 	let $description = $('<textarea>',{rows :'3', name: 'description', 
	 		'class': 'taskDescription details', text: aTask.description});
	 	let $date = $('<input>', { type : 'text', name: 'validTill', 
	 		'class': 'taskValidTill details', value: aTask.dateExpires});
	 	let $color = $('<input>', { type : 'color', name : 'color', 
	 		'class': 'taskColor details', value : aTask.color});
	 	let $save = $('<input>', {type : 'submit', value : 'Save', name : 'saveDetails'});
	 	$date.datepicker();
	 	$description.appendTo($form);
	 	$date.appendTo($form);
	 	$color.appendTo($form);
	 	$save.appendTo($form);	 	

	 	return $form;
 	},
 	/**
 	 * Builds a div where task details are displayed after task is 
 	 * edited and returns the built elements as a jQuery object.
 	 */
 	buildTaskDetailViewingHTML : function(aTask){
 		let prop = [aTask.description, aTask.dateExpires];
 		let $container = $('<div>',{'class': 'taskDetailsContainer'});
 		let dateText = aTask.dateExpires !== '' ? 'Skończ do: '+aTask.dateExpires : '';

 		$('<span>',{'class': 'details', text: aTask.description}).appendTo($container);
 		$('<span>',{'class': 'details', text: dateText}).appendTo($container);
 		
 		return $container;
 	}
 	
 };
/**
 * taskHandler is a public object which stores actions related
 * to manipulating tasks or manipulating elements within tasks
 */
 var taskHandler = {
		 
	 /**
	  * Uses task builder to build a new task container and
	  * adds it to desired DOM element. Fills the built task
	  * with aTask.text
	  */
 	addNewToContainer : function(aTask, $container){
 		let $me = taskBuilder.buildNewTaskHTML(aTask);
 		$me.hide();
 		$me.appendTo($container);
 		$me.fadeIn('fast');
 	},
 	/**
 	 * Removes a task jQuery object from its container
 	 */
 	removeFromContainer : function($task){
 		$task.fadeOut('fast', function(){ $( this ).remove(); }); 		
 	},
 	/**
 	 * Moves a task jQuery object to new container
 	 */
 	moveToNewContainer : function($task, $newContainer){
 		$task.fadeOut('fast', function(){
 			let $me = $(this).detach();
 			$me.children('a.taskComplete').remove();
			$me.appendTo($newContainer);
			$me.fadeIn('slow');
 		});
 	},
 	/**
 	 * Removes task's edit form. $container identifies the
 	 * task element outer div
 	 */
 	removeEditForm : function($container){
 		let $form = $container.find('form.taskDetailsForm').eq(0);
 		$form.fadeOut('fast', function(){ $( this ).remove();});
 		//$form.remove();
 	},
 	/**
 	 * Adds task's edit form to task container identified by 
 	 * $container
 	 */
 	addEditForm : function(aTask, $container){
 		let $me = taskBuilder.buildTaskEditForm(aTask);
 		let $taskDetails = $container.children('div.taskDetails').eq(0);
 		$me.hide();
 		$me.appendTo($taskDetails);
 		$me.slideDown('slow', 
 			function(){ $( this ).children('textarea').focus();	});

 	},
 	/**
 	 * Removes task details panel from task div. $container
 	 * identifies the task div.
 	 */
 	removeViewingPanel : function($container){
 		let $me = $container.find('div.taskDetailsContainer').eq(0);
 		$me.fadeOut('fast', function(){ $me.remove(); }); 		
 	},
 	/**
 	 * Adds task details panel to desired container. TaTask identifies
 	 * a task for which the panel is being added. $container is a top
 	 * level task container
 	 */
 	addViewingPanel : function(aTask, $container){
 		let $me = taskBuilder.buildTaskDetailViewingHTML(aTask);
 		$me.hide();
 		let $taskDetails = $container.children('div.taskDetails').eq(0);
 		$me.appendTo($taskDetails);
 		$me.slideDown('slow');
 	}, 
 	/**
 	 * Removes edit form, replacing it with viewing panel or the other 
 	 * way round. 
 	 */
 	togglePanels : function(aTask, $container){
 		$form = $container.find('form.taskDetailsForm');
 		if($form.length > 0){
 			$form.slideUp('fast', 
 				function(){ 
 					$( this ).remove();
 					taskHandler.addViewingPanel(aTask, $container);
 				});
 		}else{
 			let $viewingPanel = $container.find('div.taskDetailsContainer').eq(0);
 			if($viewingPanel.length > 0){
 			$viewingPanel.slideUp('fast', 
 				function(){ 
 					$(this).remove(); 
 					taskHandler.addEditForm(aTask,$container);
 				}); 
 			}else{
 				taskHandler.addEditForm(aTask,$container);
 			}
 		}
 	}
 }
